<?php

/**
 * @file
 * Epsilon function for webservice integration.
 */

/**
 * Class responsible for Epsilon Integration.
 */
class EpsilonIntegration {

  /**
   * Class Constants.
   *
   * @var tag
   */
  const TIMESTAMP_SIZE = 23;

  const EPSILON_SERVICE_SUCCESS = '1';

  private $wsdl;

  private $partner;

  private $client;

  private $folder;

  private $template;

  private $plist;

  private $username;

  private $password;

  private $passwordEncryptHash = 'SHA256';

  private $serviceResponse;

  private $requestTimestamp;

  private $emailTo;

  /**
   * Class contructor responsible for creating new EpsilonIntegration objects.
   */
  public function __construct() {
    $module_path = drupal_get_path('module', 'epsilon_connector');
    require_once $module_path . '/lib/nusoap/nusoap.php';

    $this->wsdl = variable_get('epsilon_connector_webservice_wsdl');
    $this->partner = variable_get('epsilon_connector_webservice_partner');
    $this->client = variable_get('epsilon_connector_webservice_client');
    $this->folder = variable_get('epsilon_connector_webservice_folder');
    $this->plist = variable_get('epsilon_connector_webservice_plist');
    $this->username = variable_get('epsilon_connector_webservice_username');
    $this->password = variable_get('epsilon_connector_webservice_password');
  }

  /**
   * Generates the hashed password for epsilon request.
   */
  private function generatePassword() {
    $timestamp = $this->getTimestamp();
    $request_password = $timestamp . $this->password;

    return hash($this->passwordEncryptHash, $request_password);
  }

  /**
   * Returns the timestamp in the format expected to be used in epsilon request.
   */
  private function getTimestamp() {
    if (!$this->requestTimestamp) {
      $date = DateTime::createFromFormat('U.u', microtime(TRUE));
      $this->requestTimestamp = substr($date->format('Y-m-d\TH:i:s.u'), 0, EpsilonIntegration::TIMESTAMP_SIZE);
    }

    return $this->requestTimestamp;
  }

  /**
   * Integrates with DREAM Epsilon to send email.
   */
  public function sendEmail($emailTo, $tokens = array()) {
    $this->emailTo = $emailTo;

    $client = new nusoap_client($this->wsdl, 'wsdl');

    $client_error = $client->getError();
    if ($client_error) {
      return FALSE;
    }

    $client->namespaces = array(
      'SOAP-ENV' => 'http://schemas.xmlsoap.org/soap/envelope/',
      'dre' => 'DREAMRTMWebletReq.xsd',
    );

    $requestBody = $this->getRequestBody($emailTo, $tokens);

    $client->call('MakeTriggerRequest', $requestBody);
    $response = $client->responseData;

    $this->serviceResponse = $response;

    return $this->getResponseCode();
  }

  /**
   * Generates request body.
   */
  public function getRequestBody($emailTo, $tokens = array()) {
    $requestBody = new SimpleXMLElement('<RTMTriggerRequest/>');

    $header = $requestBody->addChild('Header');
    $header->addChild('Partner', $this->partner);
    $header->addChild('Client', $this->client);
    $header->addChild('Timestamp', $this->getTimestamp());
    $header->addChild('Username', $this->username);
    $header->addChild('Password', $this->generatePassword());
    $header->addChild('Hash', $this->passwordEncryptHash);

    $triggerTemplate = $requestBody->addChild('RTMTriggerTemplate');

    $emailTarget = $triggerTemplate->addChild('TriggerEmailTarget');
    $emailTarget->addChild('Folder', $this->folder);
    $emailTarget->addChild('Template', $this->template);

    $toEmailAddress = $emailTarget->addChild('ToEmailAddress');

    $eventEmailAddress = $toEmailAddress->addChild('EventEmailAddress');
    $eventEmailAddress->addChild('EmailAddress', $emailTo);

    if (count($tokens) > 0) {
      $eventVariables = $eventEmailAddress->addChild('EventVariables');

      foreach ($tokens as $key => $value) {
        $variable = $eventVariables->addChild('Variable');
        $variable->addChild('Name', $key);
        $variable->addChild('Value', $value);
      }
    }

    $xmlBody = $requestBody->asXML();

    $xmlBody = str_replace('<?xml version="1.0"?>', '', $xmlBody);
    $xmlBody = str_replace('RTMTriggerRequest', 'dre:RTMTriggerRequest', $xmlBody);

    return $xmlBody;
  }

  /**
   * Returns the response of a request.
   */
  public function getServiceResponse() {
    return $this->serviceResponse;
  }

  /**
   * Extract the Code from Epsilon response.
   */
  private function getResponseCode() {
    try {
      $doc = new DOMDocument();
      $doc->loadXML($this->serviceResponse);

      return $doc->getElementsByTagName('Code')->item(0)->nodeValue;
    }
    catch (Exception $ex) {
      return FALSE;
    }
  }

  /**
   * Evaluates and Epsilon response and saves it on watchdog.
   */
  public function logResquestOnWatchdog() {
    // If there is no response to evaluate, return.
    if (!$this->getServiceResponse()) {
      return FALSE;
    }

    if ($this->getResponseCode() == EpsilonIntegration::EPSILON_SERVICE_SUCCESS) {
      $log_message = 'Message has been sent to Epsilon service successfully. Email: %email';
      $log_variables = array(
        '%email' => $this->emailTo,
      );
      $log_type = WATCHDOG_INFO;
    }
    else {
      $log_message = 'Error on Epsilon service integration. Email: %email. Webservice response: %ws_response';
      $log_variables = array(
        '%email' => $this->emailTo,
        '%ws_response' => $this->getServiceResponse(),
      );
      $log_type = WATCHDOG_ERROR;
    }

    watchdog('epsilon_connector', $log_message, $log_variables, $log_type);
  }

  /**
   * Debug function.
   */
  private function epsilonDebug($var) {
    $log_message = 'Debug: %var';
    $log_variables = array(
      '%var' => $var,
    );
    $log_type = WATCHDOG_DEBUG;
    watchdog('epsilon_connector', $log_message, $log_variables, $log_type);
  }

  /**
   * Function responsible for setting up a new template.
   */
  public function setTemplate($newTemplate) {
    $this->template = $newTemplate;
  }

}
