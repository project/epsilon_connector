<?php

/**
 * @file
 * Administrative page callbacks for the DREAM integration module.
 */

/**
 * Implements hook_form() - Responsible for the webservice configuration tab.
 */
function epsilon_connector_admin_webservice_config_form() {
  $form = array();

  $form['epsilon_connector_webservice']['epsilon_connector_webservice_wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('Web Service WSDL'),
    '#default_value' => variable_get('epsilon_connector_webservice_wsdl'),
    '#size' => 50,
    '#required' => TRUE,
  );

  $form['epsilon_connector_webservice']['epsilon_connector_webservice_partner'] = array(
    '#type' => 'textfield',
    '#title' => t('DREAM Partner'),
    '#default_value' => variable_get('epsilon_connector_webservice_partner'),
    '#size' => 50,
    '#required' => TRUE,
    '#description' => t('A collection of different business lines for an organization that share common properties in DREAM. </br> This is the highest level in the DREAM hierarchy.'),
  );

  $form['epsilon_connector_webservice']['epsilon_connector_webservice_client'] = array(
    '#type' => 'textfield',
    '#title' => t('DREAM Client'),
    '#default_value' => variable_get('epsilon_connector_webservice_client'),
    '#size' => 50,
    '#required' => TRUE,
    '#description' => t('Delineates a line of business, product or service, or the like under the partner. </br> Clients can represent any type of business, such as a website, a publication, or a brick and mortar business.'),
  );

  $form['epsilon_connector_webservice']['epsilon_connector_webservice_plist'] = array(
    '#type' => 'textfield',
    '#title' => t('DREAM Profile List Name'),
    '#default_value' => variable_get('epsilon_connector_webservice_plist'),
    '#size' => 50,
    '#required' => TRUE,
    '#description' => t('A collection of subscriber profiles maintained in DREAM to which email messages are mailed.'),
  );

  $form['epsilon_connector_webservice']['epsilon_connector_webservice_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('DREAM Folder'),
    '#default_value' => variable_get('epsilon_connector_webservice_folder'),
    '#size' => 50,
    '#required' => TRUE,
    '#description' => t('A location in DREAM that houses all of the assets related to a specific campaign.'),
  );

  $form['epsilon_connector_webservice']['epsilon_connector_webservice_username'] = array(
    '#type' => 'textfield',
    '#title' => t('DREAM Username'),
    '#default_value' => variable_get('epsilon_connector_webservice_username'),
    '#size' => 50,
    '#required' => TRUE,
    '#description' => 'Username provided by Epsilon for RTM HTTP requests.'
  );

  $form['epsilon_connector_webservice']['epsilon_connector_webservice_password'] = array(
    '#type' => 'textfield',
    '#title' => t('DREAM Password'),
    '#default_value' => variable_get('epsilon_connector_webservice_password'),
    '#size' => 50,
    '#required' => TRUE,
    '#description' => t('User password provided by Epsilon for RTM HTTP requests.'),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form() - Responsible for the webform configuration tab.
 */
function epsilon_connector_admin_webform_config_form() {
  module_load_include('inc', 'epsilon_connector', 'inc/epsilon_connector.utils');

  $form = array();
  $form['#validate'][] = 'epsilon_connector_webform_config_validate';

  $available_webforms = epsilon_connector_get_available_webforms();

  if (empty($available_webforms)) {
    // If does not exist any available webform, a warning message will be displayed.
    $text_message = 'Does not exist any Webform. Please create at least one to configure the DREAM integration.';
    $type = 'warning';
    drupal_set_message(t($text_message), $type, FALSE);
    return system_settings_form($form);
  }

  foreach ($available_webforms as $key => $value) {
    $form['epsilon_connector_webform']['epsilon_integration_enabled_' . $value->nid] = array(
      '#type' => 'fieldset',
      '#title' => $value->title . ' [nid: ' . $value->nid . ']',
      '#weight' => $value->nid,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['epsilon_connector_webform']['epsilon_integration_enabled_' . $value->nid]['epsilon_integration_enabled_' . $value->nid] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Epsilon integration'),
      '#default_value' => variable_get('epsilon_integration_enabled_' . $value->nid, FALSE),
    );
    $form['epsilon_connector_webform']['epsilon_integration_enabled_' . $value->nid]['epsilon_integration_emails_count_' . $value->nid] = array(
      '#type' => 'textfield',
      '#title' => t('E-mails templates count'),
      '#default_value' => variable_get('epsilon_integration_emails_count_' . $value->nid, FALSE),
      '#size' => 50,
      '#description' => t('The number of email templates this form will be able to trigger.'),
      '#states' => array(
        'required' => array(
          ':input[name="epsilon_integration_enabled_' . $value->nid . '"]' => array('checked' => TRUE),
        )
      )
    );
  }

  return system_settings_form($form);
}

/**
 * Implements the specific field validation function for the webform configuration tab.
 */
function epsilon_connector_webform_config_validate($node, $form) {
  module_load_include('inc', 'epsilon_connector', 'inc/epsilon_connector.utils');

  $available_webforms = epsilon_connector_get_available_webforms();

  foreach ($available_webforms as $key => $value) {

    // Verifies if the webform has been enabled for integration.
    if ($form['input']['epsilon_integration_enabled_' . $value->nid] == 0) {
      continue;
    }

    // If the e-mail templates count is empty or zero, an error message will be displayed.
    if (empty($form['input']['epsilon_integration_emails_count_' . $value->nid])) {
      $error_message = 'E-mails templates count for "' . $value->title . '" webform is required.';
      form_set_error('epsilon_integration_emails_count_' . $value->nid, t($error_message));
    }

    epsilon_connector_value_is_numeric_greater_than_zero($form, $value);
  }
}

/**
 * Verifies if the submitted value is numeric and greater then zero.
 */
function epsilon_connector_value_is_numeric_greater_than_zero($form, $webform) {

  // If the user submitted a value that is not numeric, an error message will be displayed.
  if (!is_numeric($form['input']['epsilon_integration_emails_count_' . $webform->nid])) {
    $error_message = 'E-mails templates count for "' . $webform->title . '" webform must be numeric.';
    form_set_error('epsilon_integration_emails_count_' . $webform->nid, t($error_message));
  }

  // If the e-mail templates count is less then zero, an error message will be displayed.
  if ($form['input']['epsilon_integration_emails_count_' . $webform->nid] < 0) {
    $error_message = 'E-mails templates count for "' . $webform->title . '" must be greater than zero.';
    form_set_error('epsilon_integration_emails_count_' . $webform->nid, t($error_message));
  }
}

/**
 * Implements hook_form() - Responsible for e-mails templates tab.
 */
function epsilon_connector_admin_emails_templates_form() {
  module_load_include('inc', 'epsilon_connector', 'inc/epsilon_connector.utils');

  $form = array();
  $form['#validate'][] = 'epsilon_connector_emails_templates_validate';

  $available_webforms = epsilon_connector_get_available_webforms();

  if (empty($available_webforms)) {
    // If does not exist any available webform, a warning message will be displayed.
    $text_message = 'Does not exist any Webform. Please create at least one to configure the DREAM integration.';
    $type = 'warning';
    drupal_set_message(t($text_message), $type, FALSE);
    return system_settings_form($form);
  }

  $enabled_webforms = epsilon_connector_get_enabled_webforms();

  if (empty($enabled_webforms)) {
    // If exists at least one webform but no configuration has been set, a warning message will be displayed.
    $text_message = '<a href =' . base_path() . 'admin/config/epsilon/webform-config> Click here </a> to configure the webform(s) before setting up the e-mail templates.';
    $type = 'warning';
    drupal_set_message(t($text_message), $type, FALSE);
    return system_settings_form($form);
  }

  foreach ($enabled_webforms as $key => $value) {
    $templates_count = variable_get('epsilon_integration_emails_count_' . $key);
    $webform_title = 'Webform ' . $value;

    $form['epsilon_connector_webform_conf_' . $key] = array(
      '#type' => 'fieldset',
      '#title' => t($webform_title),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['epsilon_connector_webform_conf_' . $key]['epsilon_connector_email_field_' . $key] = array(
      '#type' => 'textfield',
      '#title' => t('E-mail Key'),
      '#default_value' => variable_get('epsilon_connector_email_field_' . $key),
      '#size' => 50,
      '#required' => TRUE,
      '#description' => t('The e-mail field key where the final users will input their information to receive the RTM message.'),
    );

    for ($i = 1; $i <= $templates_count; $i++) {
      $template_title = 'Template ' . $i;

      $form['epsilon_connector_webform_conf_' . $key]['epsilon_connector_template_conf_' . $i] = array(
        '#type' => 'fieldset',
        '#title' => t($template_title),
        '#weight' => $i,
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['epsilon_connector_webform_conf_' . $key]['epsilon_connector_template_conf_' . $i]['epsilon_connector_webservice_template_' . $key . '_' . $i] = array(
        '#type' => 'textfield',
        '#title' => t('DREAM Template'),
        '#default_value' => variable_get('epsilon_connector_webservice_template_' . $key . '_' . $i),
        '#size' => 50,
        '#required' => TRUE,
        '#description' => t('Specifies the RTM message (an email marketing message) to trigger.'),
      );

      $form['epsilon_connector_webform_conf_' . $key]['epsilon_connector_template_conf_' . $i]['epsilon_connector_webservice_conditional_fields_' . $key . '_' . $i] = array(
        '#type' => 'textarea',
        '#title' => t('Conditional Fields'),
        '#default_value' => variable_get('epsilon_connector_webservice_conditional_fields_' . $key . '_' . $i),
        '#description' => t('<strong>Key-value pairs MUST be specified as "Field_ID|Field_value"</strong>. Use of only alphanumeric characters and underscores is recommended in keys. One option per line.'),
      );

      $form['epsilon_connector_webform_conf_' . $key]['epsilon_connector_template_conf_' . $i]['epsilon_connector_webservice_template_tokens_' . $key . '_' . $i] = array(
        '#type' => 'textarea',
        '#title' => t('Tokens'),
        '#default_value' => variable_get('epsilon_connector_webservice_template_tokens_' . $key . '_' . $i),
        '#description' => t('<strong>Key-value pairs MUST be specified as "DREAM_token_id|Webform_field_id"</strong>. Use of only alphanumeric characters and underscores is recommended in keys. One option per line.'),
      );
    }
  }

  return system_settings_form($form);
}

/**
 * Gets the webform fields.
 */
function epsilon_connector_get_webform_fields_and_types($webform_id) {
  // Load the node related to the current webform key.
  $node = node_load($webform_id);
  $fields = array();

  // Value validation will occurr only for the following field types: checkbox, radio and select.
  $field_types = array('checkbox', 'radio', 'select');

  // Find all the webform components.
  foreach ($node->webform['components'] as $component_key => $component) {

    // If the component does not match the checkbox, radio or select types, it does not have any default values.
    if (!in_array($component['type'], $field_types)) {
      $fields[$component['form_key']] = $component['type'];
      continue;
    }

    $fields[$component['form_key']]['type'] = $component['type'];
    $items_array = list_extract_allowed_values($component['extra']['items'], 'list_text', FALSE);

    // Scrolls through all the possible values of the component.
    foreach ($items_array as $item_id => $item_value) {
      $fields[$component['form_key']][$item_id] = $item_value;
    }
  }

  return $fields;
}

/**
 * Implements the specific field validation function for the emails templates tab.
 */
function epsilon_connector_emails_templates_validate($form, &$form_state) {
  module_load_include('inc', 'epsilon_connector', 'inc/epsilon_connector.utils');

  $enabled_webforms = epsilon_connector_get_enabled_webforms();

  foreach ($enabled_webforms as $webform_id => $webform_title) {

    // Validates the e-mail field id.
    epsilon_connector_validate_email_field_id($webform_id, $webform_title, $form_state);

    // Validates the conditional fields.
    epsilon_connector_validate_conditional_fields($webform_id, $webform_title, $form_state);

    // Validates the tokens.
    epsilon_connector_validate_tokens($webform_id, $webform_title, $form_state);
  }
}

/**
 * Validates the e-mail field id.
 */
function epsilon_connector_validate_email_field_id($webform_id, $webform_title, $form_state) {
  $email_field_submitted = $form_state['input']['epsilon_connector_email_field_' . $webform_id];

  // If the value submitted by the user does not match any webform field, an error message will be displayed.
  if (!epsilon_connector_field_id_exists_in_webform($email_field_submitted, $webform_id)) {
    $error_message = 'The "' . $webform_title . '" webform does not contain the "' . $email_field_submitted . '" field.';
    form_set_error('epsilon_connector_email_field_' . $webform_id, t($error_message));
    return;
  }

  $field_type = epsilon_connector_get_field_type($email_field_submitted, $webform_id);

  // Verifies if the typed field is an e-mail.
  if ($field_type == "email") {
    return;
  }

  // If the field type is not an e-mail, then a warning message will be displayed.
  $text_message = 'The "' . $email_field_submitted . '" field is a "' . $field_type . '", not the E-mail type. Make sure to use the correct field.';
  $type = 'warning';
  drupal_set_message(t($text_message), $type, FALSE);
}

/**
 * Returns the field type.
 */
function epsilon_connector_get_field_type($field_id, $webform_id) {
  $fields = epsilon_connector_get_webform_fields_and_types($webform_id);

  if (is_array($fields[$field_id])) {
    return $fields[$field_id]['type'];
  }

  return $fields[$field_id];
}

/**
 * Verifies if the webform contains a certain field id.
 */
function epsilon_connector_field_id_exists_in_webform($field_id, $webform_id) {

  // Verifies if the current webform contains any component with the same name as the submitted by the user.
  if (in_array($field_id, array_keys(epsilon_connector_get_webform_fields_and_types($webform_id)))) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Validates the conditional fields.
 */
function epsilon_connector_validate_conditional_fields($webform_id, $webform_title, $form_state) {
  $templates_count = variable_get('epsilon_integration_emails_count_' . $webform_id);

  for ($i = 1; $i <= $templates_count; $i++) {
    $has_invalid_conditional_fields = FALSE;
    $conditional_fields = $form_state['input']['epsilon_connector_webservice_conditional_fields_' . $webform_id . '_' . $i];

    // Verifies if there is any conditional field configured to the current template.
    if (!$conditional_fields) {
      break;
    }

    $conditional_fields_array = list_extract_allowed_values($conditional_fields, 'list_text', FALSE);

    $error_message = 'The "' . $webform_title . '" webform (Template ' . $i . ') has invalid conditional fields: </br><ol>';

    // Verifies if any conditional field is invalid.
    foreach ($conditional_fields_array as $field_id => $field_value) {

      // Validates the conditional field id.
      if (!epsilon_connector_field_id_exists_in_webform($field_id, $webform_id)) {
        $error_message = $error_message . '<li>Field ID: "' . $field_id . '"</li>';
        $has_invalid_conditional_fields = TRUE;

        // If the conditional field id is invalid, the field value will be invalid too.
        continue;
      }

      // Validates the conditional field value.
      if (!epsilon_connector_field_value_exists_in_webform($field_id, $field_value, $webform_id)) {
        $error_message = $error_message . '<li>Field Value: "' . $field_value . '"</li>';
        $has_invalid_conditional_fields = TRUE;
      }
    }

    $error_message = $error_message . '</ol>';

    // If the current conditional field is invalid, an error message will be displayed.
    if ($has_invalid_conditional_fields) {
      form_set_error('epsilon_connector_webservice_conditional_fields_' . $webform_id . '_' . $i, t($error_message));
    }
  }
}

/**
 * Verifies if the webform contains a certain field value.
 */
function epsilon_connector_field_value_exists_in_webform($field_id, $field_value, $webform_id) {

  $value_exists = FALSE;

  // Verifies if the current webform contains any component with the same value as the submitted by the user.
  foreach (epsilon_connector_get_webform_fields_and_types($webform_id) as $field_key => $values) {
    if ($field_key <> $field_id) {
      continue;
    }

    // If the field type is not checkbox, radio or select, the field value does not matter.
    if (!is_array($values)) {
      $value_exists = TRUE;
      continue;
    }

    // Scrolls through all the possible values of the component.
    foreach ($values as $item_id => $item_value) {
      if ($item_value == $field_value) {
        $value_exists = TRUE;
        break;
      }
    }
  }

  return $value_exists;
}

/**
 * Validates the tokens.
 */
function epsilon_connector_validate_tokens($webform_id, $webform_title, $form_state) {
  $templates_count = variable_get('epsilon_integration_emails_count_' . $webform_id);

  for ($i = 1; $i <= $templates_count; $i++) {
    $has_invalid_tokens = FALSE;
    $tokens = $form_state['input']['epsilon_connector_webservice_template_tokens_' . $webform_id . '_' . $i];

    // Verifies if there is any token configured to the current template.
    if (!$tokens) {
      continue;
    }

    $tokens_array = list_extract_allowed_values($tokens, 'list_text', FALSE);

    $error_message = 'The "' . $webform_title . '" webform (Template ' . $i . ') has invalid tokens: </br><ol>';

    // Verifies if any token is invalid.
    foreach ($tokens_array as $field_id => $field_value) {
      if (!epsilon_connector_field_id_exists_in_webform($field_value, $webform_id)) {
        $error_message = $error_message . '<li>Webform Field ID: "' . $field_value . '"</li>';
        $has_invalid_tokens = TRUE;
      }
    }

    $error_message = $error_message . '</ol>';

    // If the current token is invalid, an error message will be displayed.
    if ($has_invalid_tokens) {
      form_set_error('epsilon_connector_webservice_template_tokens_' . $webform_id . '_' . $i, t($error_message));
    }
  }
}
