<?php

/**
 * @file
 * Provides integration with DREAM real-time messaging service (Epsilon).
 */

/**
 * Implements hook_menu().
 */
function epsilon_connector_menu() {
  $items['admin/config/epsilon'] = array(
    'title' => 'Epsilon Configuration',
    'position' => 'left',
    'weight' => 0,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer epsilon config'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  $items['admin/config/epsilon/webservice-config'] = array(
    'title' => 'Webservice Configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('epsilon_connector_admin_webservice_config_form'),
    'access arguments' => array('administer epsilon config'),
    'weight' => 1,
    'type'  => MENU_LOCAL_TASK,
    'file' => 'inc/epsilon_connector.admin.inc',
  );

  $items['admin/config/epsilon/webform-config'] = array(
    'title' => 'Webform Configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('epsilon_connector_admin_webform_config_form'),
    'access arguments' => array('administer epsilon config'),
    'weight' => 2,
    'type'  => MENU_LOCAL_TASK,
    'file' => 'inc/epsilon_connector.admin.inc',
  );

  $items['admin/config/epsilon/emails-config'] = array(
    'title' => 'Emails Templates',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('epsilon_connector_admin_emails_templates_form'),
    'access arguments' => array('administer epsilon config'),
    'weight' => 3,
    'type'  => MENU_LOCAL_TASK,
    'file' => 'inc/epsilon_connector.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function epsilon_connector_permission() {
  return array(
    'administer epsilon config' => array(
      'title' => t('Administer Epsilon Configuration'),
      'description' => t('Provide access epsilon configuration screen.'),
    ),
  );
}

/**
 * Implements hook_form_alter().
 */
function epsilon_connector_form_alter(&$form, &$form_state, $form_id) {
  module_load_include('inc', 'epsilon_connector', 'inc/epsilon_connector.utils');

  $enabled_webforms = epsilon_connector_get_enabled_webforms();

  // Foreach webform enabled, a DREAM integration will be established.
  foreach ($enabled_webforms as $key => $value) {
    if ($form_id == 'webform_client_form_' . $key) {
      $form['#submit'][] = 'epsilon_connector_dream_integration';
    }
  }
}

/**
 * Custom submit function to execute the DREAM Integration.
 */
function epsilon_connector_dream_integration($form, &$form_state) {
  module_load_include('inc', 'epsilon_connector', 'inc/epsilon_connector.service');

  // Gets the recipient's e-mail field id.
  $email_field = variable_get('epsilon_connector_email_field_' . $form['#node']->nid);

  // If the end user had typed an e-mail, the DREAM Integration will be triggered.
  if (empty($form_state['input']['submitted'][$email_field])) {
    return;
  }

  $email_to = $form_state['input']['submitted'][$email_field];

  // Number of e-mail templates for the current webform.
  $templates_count = variable_get('epsilon_integration_emails_count_' . $form['#node']->nid);

  // A certain template will be selected through the final user's responses.
  for ($i = 1; $i <= $templates_count; $i++) {

    // The final user must answer all the Webform questions by following the conditional fields. So that, the current template will be sent.
    if (epsilon_connector_supplies_all_conditions($form['#node']->nid, $i, $form_state)) {
      $epsilon = new EpsilonIntegration();

      // Set the correct e-mail template.
      $epsilon->setTemplate(variable_get('epsilon_connector_webservice_template_' . $form['#node']->nid . '_' . $i));
      $epsilon->sendEmail($email_to, epsilon_connector_get_tokens($form['#node']->nid, $i, $form_state));

      // Log the request result on watchdog.
      $epsilon->logResquestOnWatchdog();
    }
  }
}

/**
 * Responsible for validating if the template follows all the conditions.
 */
function epsilon_connector_supplies_all_conditions($webform_id, $template_id, &$form_state) {
  $conditional_fields = variable_get('epsilon_connector_webservice_conditional_fields_' . $webform_id . '_' . $template_id);
  $conditional_fields_array = array();
  $supplied_all_conditions = TRUE;

  // Verifies if there is any conditional field configured to this template.
  if ($conditional_fields) {
    $conditional_fields_array = list_extract_allowed_values($conditional_fields, 'list_text', FALSE);
  }

  foreach ($conditional_fields_array as $field_id => $field_value) {
    // If the user has not answered a question as expected by the the conditional fields.
    if (strtoupper($field_value) <> strtoupper($form_state['input']['submitted'][$field_id])) {
      $supplied_all_conditions = FALSE;
    }
  }

  return $supplied_all_conditions;
}

/**
 * Extracts the available tokens.
 */
function epsilon_connector_get_tokens($webform_id, $template_id, $form_state) {
  $tokens = variable_get('epsilon_connector_webservice_template_tokens_' . $webform_id . '_' . $template_id);
  $tokens_array = array();

  // If the current template requires any token.
  if (!$tokens) {
    return $tokens_array;
  }

  $tokens_array = list_extract_allowed_values($tokens, 'list_text', FALSE);

  // The webform id will be updated for the submitted value.
  foreach ($tokens_array as $token_dream_id => $token_webform_id) {
    $tokens_array[$token_dream_id] = $form_state['input']['submitted'][$token_webform_id];
  }

  return $tokens_array;
}
